import java.util.Scanner;

public class Problem3 {
    // เขียนโปรแกรมรับตัวเลข 2 ตัว โดยที่ตัวแรกจะต้องน้อยกว่าหรือเท่ากับตัวหลังเสมอ
    // หากไม่ตรงตามเงื่อนไขจะพิมพ์ว่า Error
    // หากตรงตามเงื่อนไขจะพิมพ์ตัวเลขที่อยู่ระหว่างตัวเลขทั้งสองออกมา
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        Scanner sc2 = new Scanner(System.in);

        int Fst;
        int Snd;

        while (true) {
            System.out.print("Plese input first number: ");
            Fst = sc1.nextInt();
            System.out.print("Plese input second number: ");
            Snd = sc2.nextInt();
            if (Fst > Snd)
            System.out.println("Error");
            for (int i = Fst; i < Snd + 1; i++) {
                System.out.println(i);
            }
        }
    }
}
